﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

	Rigidbody rb;
	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody> ();
	}
	
	// Update is called once per frame
	void Update () {
		Move ();
	}

	void Move(){
		
		rb.AddForce(new Vector3(10*Input.GetAxis("Horizontal"), 0, 0));
		rb.AddForce(new Vector3(0, 0, 10*Input.GetAxis("Vertical")));
	}

}



