﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

	private Rigidbody rb;
	public GameObject []pontos;
	private int state;
	private int targetIndex;
	private Vector3 target;


	/*1 search
	 * 2 stalk
	 * 3 stop
	 */

	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody> ();
		pontos = GameObject.FindGameObjectsWithTag ("Pontos");		//Pegar todas as bolinhas
		state = 1;													//estado inicial da máquina 
	}
	
	// Update is called once per frame
	void Update () {
		//print (state);
		Decision ();	//máquina						
	}

	void Decision(){	//máquina

		switch (state) {
		case 1:			//se o estado for 1, então ele faz a busca pela bolinha mais próxima
			
			target = pontos[0].transform.position;	//guarda a posição da primeira bolinha

			for (int i = 1; i < pontos.Length; i++) {	//pra cada bolinha que existe, se a distância for menor que o alvo atual, essa bolinha será o novo alvo
				if(pontos[i].gameObject.activeSelf && i != targetIndex){
					
					if ((Vector3.Distance(pontos [i].transform.position, this.transform.position) < Vector3.Distance(target, this.transform.position))) {
						print (pontos [i].name);
						target = pontos [i].transform.position;
						targetIndex = i;
					}
				}
			}
			state = 2;	//depois que encontrar a bolinha mais próxima, a máquina muda de estado
			break;
		case 2:
			Move ();
			break;
		}
	}

	void Move(){

		if(target.x < this.transform.position.x)
			rb.AddForce(new Vector3(-10, 0, 0));
		else 
			rb.AddForce(new Vector3(10, 0, 0));
		
		if(target.z < this.transform.position.z)
			rb.AddForce(new Vector3(0, 0, -10));
		else 
			rb.AddForce(new Vector3(0, 0, 10));

		if (!pontos[targetIndex].gameObject.activeSelf) {			
			state = 1;
		}
			
	}

}



